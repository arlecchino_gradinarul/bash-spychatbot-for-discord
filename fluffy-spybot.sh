clear
border="----------------------------------------------------"
date="Romania"
logo() {
	echo ""
	echo "  ███████╗██╗     ██╗   ██╗███████╗███████╗██╗   ██╗"
	echo "  ██╔════╝██║     ██║   ██║██╔════╝██╔════╝╚██╗ ██╔╝"
	echo "  █████╗  ██║     ██║   ██║█████╗  █████╗   ╚████╔╝ "
	echo "  ██╔══╝  ██║     ██║   ██║██╔══╝  ██╔══╝    ╚██╔╝  "
	echo "  ██║     ███████╗╚██████╔╝██║     ██║        ██║   "
	echo "  ╚═╝     ╚══════╝ ╚═════╝ ╚═╝     ╚═╝        ╚═╝   "
	echo "                 Arlecch1no@Gradinarul- 2019"
	echo "                              TheGardener"
}

logo

check_if_root() {
	if [[ "$(whoami)" == "root" ]];
	then
		echo "| $date ∣ User Root  [DONE]"
	else
		echo "| $date ∣ User Root [PROBLEM]"
		echo "| $date ∣ Are u root ?"
		exit
	fi
}


# Check values from config.txt 

echo "----------------------------------------------------"
echo "|"
email=$(cat config.txt | grep "<fluffy_email>" | sed "s/<fluffy_email>//g" | sed -n 1p)
echo "| $date ∣ Username: [Hidden]"
password=$(cat config.txt | grep "<fluffy_password>" | sed "s/<fluffy_password>//g" | sed -n 1p)
echo "| $date ∣ Password: [Hidden]"
room=$(cat config.txt | grep "<fluffy_spy_room>" | sed "s/<fluffy_spy_room>//g" | sed -n 1p)
echo "| $date ∣ RoomID: "$room
hook=$(cat config.txt | grep "<fluffy_url_hook>" | sed "s/<fluffy_url_hook>//g" | sed -n 1p)
hook_room=$(cat config.txt | grep "<fluffy_hook_room>" | sed "s/<fluffy_hook_room>//g" | sed -n 1p)
echo "| $date ∣ HookRoom: "$hook_room
echo "|"

if [[ $(echo "$email" | wc -c) > 1 ]];
then
	echo "| $date | [DONE] ∣ Status for username"
else
	echo "| $date ∣ [FAIL] | Status for username"
	echo "|"
	echo $border
	exit
fi

if [[ $(echo "$password" | wc -c) > 1 ]];
then
	echo "| $date ∣ [DONE] ∣ Status for password"
else
	echo "| $date ∣ [FAIL] | Status for password"
	echo "|"
	echo $border
	exit
fi

if [[ $(echo "$room" | wc -c) > 1 ]];
then
	echo "| $date ∣ [DONE] ∣ Status for Room"
else
	echo "| $date ∣ [FAIL] | Status for Room"
	echo "|"
	echo $border
	exit
fi

if [[ $(echo "$hook_room" | wc -c) > 1 ]];
then
	echo "| $date ∣ [DONE] ∣ Status for HookRoom"
else
	echo "| $date ∣ [FAIL] | Status for HookRoom"
	echo "|"
	echo $border
	exit
fi

if [[ "$(which http)" =~ "/usr/local" ]];
then
	echo "| $date ∣ [DONE] ∣ Status for HTTPie "
else
	echo "| $date ∣ [FAIL] | Status for HTTPie"
	echo "|"
	echo $border
	exit
fi

if [[ "$(which ping)" =~ "/bin/" ]];
then
	echo "| $date ∣ [DONE] ∣ Status for Ping"
else
	echo "| $date ∣ [FAIL] | Status for Ping"
	echo "|"
	echo $border
	exit
fi

if [[ $(echo "$hook" | wc -c) > 1 ]];
then
	echo "| $date ∣ [DONE] ∣ Status for hook lenght"
	if [[ "$hook" =~ "https://discordapp.com/api/webhooks/" ]];
	then
		echo "| $date ∣ [DONE] ∣ Hook status"
	else
		echo "| $date ∣ [FAIL] | Hook status"

	fi
else 
	echo "| $date ∣ [FAIL] | Empty Strings ?"
	echo ""
	exit
fi
echo "|"
echo "----------------------------------------------------"

login_discord() {

	# Get Cookie
	get_cookie=$(http POST https://discordapp.com/api/v6/auth/login -h | sed -n 6p)

	# Login 
	login_tester=$(http POST https://discordapp.com/api/v6/auth/login \
	'Host: discordapp.com' \
	'Connection: close' \
	'Accept-Language: en-US' \
	'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36' \
	'Origin: https://discordapp.com' \
	'Content-Type: application/json' \
	'Accept: */*' \
	"$get_cookie" \
	'Referer: https://discordapp.com/login' \
	email=$email\
	password=$password \
	undelete=false \
	captcha_key=null \
	login_source=null)
	token=$(echo "$login_tester" | jq -r '.token')
	sleep 1
	echo "|"
	echo "| $date ∣ [OK] | Status for Token"
 
	# Check the json reponse
	if [[ "$login_tester" =~ "Password does not match." ]];
	then
		echo "| $date ∣  [Password does not match] | Status"
		exit
	elif [[ "$login_tester" =~ "Not a well formed email address" ]];
	then
		echo "| $date ∣ [Not a well formed email address] | Status"
		exit
	elif [[ "$login_tester" =~ "You are being rate limited" ]];
	then
		delay=$(echo "$login_tester" | jq -r '.retry_after')
		echo "| $date ∣ [You are being rate limited] [$delay] | Status"
		login_discord
	else
		echo "| $date ∣ [OK] | Status for Login"
	fi
}

login_discord

get_message() {

	get_last_message0=$(http GET "https://discordapp.com/api/v6/channels/$room/messages?limit=1" \
	'Connection: keep-alive' \
	"Authorization: $token" \
	'Accept-Language: en-US' \
	'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36' \
	'Accept: */*')

	if [[ "$get_last_message" =~ "401: Unauthorized" ]];
	then
		echo "| $date ∣ [Invalid Token] | Status"
		exit
	elif [[ "$get_last_message" =~ "Unknown Channel" ]];
	then
		echo "| $date ∣ [Unknown Channel] | Status"
		exit
	else
		echo "| $date ∣ [OK] | Status for Token"

	fi
	echo "|"
	echo $border
	clear

	get_hook_message=$(http GET "https://discordapp.com/api/v6/channels/$hook_room/messages?limit=1" \
	'Connection: keep-alive' \
	'Authorization: '$token \
	'Accept-Language: en-US' \
	'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36' \
	'Accept: */*')
	if [[ "$get_last_message" =~ "401: Unauthorized" ]];
	then
		echo "| $date ∣ [Invalid Token] | Status"
		exit
	elif [[ "$get_last_message" =~ "Unknown Channel" ]];
	then
		echo "| $date ∣ [Unknown Channel] | Status"
		exit
	else
		echo "| $date ∣ [OK] | Status for Token"

	fi



	# Message content

	content_0=$(echo "$get_last_message0" | jq -r .[].content)
	account_name0=$(echo "$get_last_message0" | jq -r .[].author.username)
	account_disc0=$(echo "$get_last_message0" | jq -r .[].author.discriminator)
	message_id0=$(echo "$get_last_message0" | jq -r .[].id)

	content_1=$(echo "$get_hook_message" | jq -r .[].content)
	account_name1=$(echo "$get_hook_message" | jq -r .[].author.username)
	account_disc1=$(echo "$get_hook_message" | jq -r .[].author.discriminator)
	message_id1=$(echo "$get_hook_message" | jq -r .[].id)


	# Parameter to send
	param_send="**     Username:** $account_name0
	**Disc:** $account_disc0
	**Msg:** $content_0"


	if [[ $(echo "$hook" | wc -c) > 1 ]];
	then
		echo "| $date ∣ [DONE] ∣ Status for hook lenght"
		if [[ "$hook" =~ "https://discordapp.com/api/webhooks/" ]];
		then
			echo "| $date ∣ [DONE] ∣ Hook status"
			if [[ "$content_1" =~ "$content_0" ]] && [[ "$content_1" =~ "$account_name0" ]];
			then
				clear
				echo ""
				echo " Same , ignoring..."
			else
				clear
				echo ""
				echo " Sending to hook..."
				http POST "$hook" \
				"Content-Type: application/json" content="$param_send"
				#http POST "$hook" \
				#"Content-Type: application/json" content="$param_send"
			fi
		else
			echo "| $date ∣ [FAIL] | Hook status"
		fi
	else 
		echo "| $date ∣ [FAIL] | Empty String ?"
		echo ""
		exit
	fi

}

while true;do
	get_message
done